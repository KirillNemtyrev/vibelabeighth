package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class HashSet<T> {

    private final int BUCKETS = 32;
    private final List<T>[] objects = new List[BUCKETS];

    public HashSet() {
        for (int i = 0; i < objects.length; i++) {
            objects[i] = new ArrayList<>();
        }
    }

    public void add(T t) {
        if (!objects[getBucket(t)].contains(t)) {
            objects[getBucket(t)].add(t);
        }
    }

    public boolean remove(T t) {
        return objects[getBucket(t)].remove(t);
    }

    public int size() {
        return Arrays.stream(objects).mapToInt(List::size).sum();
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public boolean contains(T t) {
        return objects[getBucket(t)].contains(t);
    }

    public Iterator<T> iterator() {
        return new HashSetIterator<>(
                Arrays.stream(objects)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toList())
        );
    }

    private int getBucket(T obj) {
        if (obj == null) return 0;
        return obj.hashCode() % BUCKETS;
    }

    static class HashSetIterator<T> implements Iterator<T> {
        private int curItem = 0;
        private final List<T> objects;

        HashSetIterator(List<T> objects) {
            this.objects = objects;
        }

        @Override
        public boolean hasNext() {
            return curItem >= objects.size();
        }

        @Override
        public T next() {
            return objects.get(curItem++);
        }
    }
}
